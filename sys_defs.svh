
`define ROB_SIZE            32
`define ROB_DATA_WIDTH      32
`define ROB_ADDR_WIDTH      $clog2(`ROB_SIZE)
`define XLEN                32
`define RF_SIZE             32
`define RF_ADDR_WIDTH       $clog2(`RF_SIZE)
`define RF_DATA_WIDTH       32

//define fetch packet
typedef struct packed{
    logic   [`XLEN-1:0]                 imem_inst_way1;
    logic   [`XLEN-1:0]                 imem_inst_way2;
} IMEM_ROB_PACKET;

//define CDB output
typedef struct packed{
    logic                               CDB_valid;
    logic   [`ROB_ADDR_WIDTH-1:0]       CDB_tag_way1;
    logic   [`ROB_ADDR_WIDTH-1:0]       CDB_tag_way2;
    logic   [`XLEN-1:0]                 CDB_rd_val_way1;
    logic   [`XLEN-1:0]                 CDB_rd_val_way2;
} CDB_ROB_PACKET;

//define disp packet
typedef struct packed{
    logic   [`XLEN-1:0]                 rs_inst_way1;
    logic   [`XLEN-1:0]                 rs_inst_way2;
    logic   [`XLEN-1:0]                 rs_rs1_val_way1;
    logic   [`XLEN-1:0]                 rs_rs2_val_way1;
    logic   [`XLEN-1:0]                 rs_rs1_val_way2;
    logic   [`XLEN-1:0]                 rs_rs2_val_way2;
} ROB_RS_PACKET;

//define map packet
typedef struct packed{
    logic   [`RF_ADDR_WIDTH-1:0]        map_rd_idx_way1;
    logic   [`RF_ADDR_WIDTH-1:0]        map_rd_idx_way2;
    logic   [`ROB_ADDR_WIDTH-1:0]       map_tag_way1;
    logic   [`ROB_ADDR_WIDTH-1:0]       map_tag_way2;
} ROB_MAP_PACKET;

//define rf packet
typedef struct packed{
    logic   [`RF_ADDR_WIDTH-1:0]        rf_rd_idx_way1;
    logic   [`RF_ADDR_WIDTH-1:0]        rf_rd_idx_way2;
    logic   [`XLEN-1:0]                 rf_rd_val_way1;
    logic   [`XLEN-1:0]                 rf_rd_val_way2;
} ROB_RF_PACKET;

//define rob
typedef struct packed{
    logic   [`XLEN-1:0]                 rob_inst;
    logic   [`XLEN-1:0]                 R;
    logic   [`XLEN-1:0]                 V;
    logic                               C;
} ROB_ENTRY;