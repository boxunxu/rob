module ROB_test();

    logic                       clk;
    logic                       resetn;
    logic                       rs_feedback;
    IMEM_ROB_PACKET             imem_rob_packet;
    CDB_ROB_PACKET              cdb_rob_packet;
    ROB_RS_PACKET               rob_rs_packet;
    ROB_MAP_PACKET              rob_map_packet;
    ROB_RF_PACKET               rob_rf_packet;

    ROB ROB(
            .clk(clk),
            .resetn(resetn),
            .rs_feedback(rs_feedback),
            .imem_rob_packet(imem_rob_packet),
            .cdb_rob_packet(cdb_rob_packet),
            .rob_rs_packet(rob_rs_packet),
            .rob_map_packet(rob_map_packet),
            .rob_rf_packet(rob_rf_packet)
            );

    always #5 clk = ~clk;

    initial begin
        clk = 0;
        resetn = 1;
        #1 resetn = 0;
        #1 resetn = 1;
        /* Test Process */
        
        $finish;
    end

endmodule