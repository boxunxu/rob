module ROB(
    input                       clk,
    input                       resetn,
    input                       rs_feedback,
    input   IMEM_ROB_PACKET     imem_rob_packet,
    input   CDB_ROB_PACKET      cdb_rob_packet,
    output  ROB_RS_PACKET       rob_rs_packet,
    output  ROB_MAP_PACKET      rob_map_packet,
    output  ROB_RF_PACKET       rob_rf_packet
);

    ROB_ENTRY   [`ROB_SIZE-1:0]         rob_entries;
    ROB_ENTRY   [`ROB_SIZE-1:0]         next_rob_entries;
    logic       [`ROB_ADDR_WIDTH-1:0]   rob_entries_head;
    logic       [`ROB_ADDR_WIDTH-1:0]   rob_entries_tail;
    logic       [`ROB_ADDR_WIDTH-1:0]   next_rob_entries_head;
    logic       [`ROB_ADDR_WIDTH-1:0]   next_rob_entries_tail;     
           
    always_comb begin
        //read rob entries(complete)
        next_rob_entries_head = rob_entries_head;
        if(rs_feedback)begin
            next_rob_entries_head = rob_entries_head + 'd2;
            //rob_rs_packet
            rob_rs_packet.rs_inst_way1 = rob_entries[rob_entries_head];
            rob_rs_packet.rs_inst_way2 = rob_entries[rob_entries_head+1];
            rob_rs_packet.rs_rs1_val_way1 = rob_entries[rob_entries_head];
            rob_rs_packet.rs_rs2_val_way1 = rob_entries[rob_entries_head];
            rob_rs_packet.rs_rs1_val_way2 = rob_entries[rob_entries_head+1];
            rob_rs_packet.rs_rs2_val_way2 = rob_entries[rob_entries_head+1];
            //rob_map_packet
            rob_map_packet.map_rd_idx_way1 = rob_entries[rob_entries_head].R;
            rob_map_packet.map_rd_idx_way2 = rob_entries[rob_entries_head+1].R;
            rob_map_packet.map_tag_way1 = rob_entries_head;
            rob_map_packet.map_tag_way2 = rob_entries_head+1;
        end
        //write rob entries when empty
        next_rob_entries_tail = rob_entries_tail;
        next_rob_entries = rob_entries;
        if(rob_entries_head - rob_entries_tail != 2)begin
            next_rob_entries[rob_entries_tail].rob_inst = imem_rob_packet.imem_inst_way1;
            next_rob_entries[rob_entries_tail+1].rob_inst = imem_rob_packet.imem_inst_way2;
        end
        //write value of rob entries
        if(cdb_rob_packet.CDB_valid)begin
            next_rob_entries[rob_entries_tail].V = cdb_rob_packet.CDB_rd_val_way1;
            next_rob_entries[rob_entries_tail+1].V = cdb_rob_packet.CDB_rd_val_way2;
            //rob_rf_packet
            rob_rf_packet.rf_rd_idx_way1 = next_rob_entries[cdb_rob_packet.CDB_tag_way1].R;
            rob_rf_packet.rf_rd_idx_way2 = next_rob_entries[cdb_rob_packet.CDB_tag_way2].R;
            rob_rf_packet.rf_rd_val_way1 = next_rob_entries[cdb_rob_packet.CDB_tag_way1].V;
            rob_rf_packet.rf_rd_val_way2 = next_rob_entries[cdb_rob_packet.CDB_tag_way2].V;
        end
    end

    always_ff @(posedge clk or negedge resetn) begin
        if(~resetn)begin
            rob_entries <= '0;
        end else begin
            rob_entries <= next_rob_entries;
        end
    end
endmodule